﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Linq;

namespace ControlTurnos.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DeferredDeletion(false)]
    [XafDefaultProperty("TurnosEmpleados")]

    public class TurnosEmpleados : XPObject, IXafEntityObject // IEvent
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        #region variables
        private DateTime _Fecha;
        private TimeSpan _HoraInicio;
        private TimeSpan _HoraFin;
        private TimeSpan _HoraInicioT;
        private TimeSpan _HoraFinT;
        private double _Cantidad;
        private ControlTurnos.Module.BusinessObjects.Temel.Employee _Empleado;
        private Incidencia _Incidencia;
        private string _Observaciones;
        private DateTime _Inicio;
        private DateTime _Fin;
        private DateTime _InicioT;
        private DateTime _FinT;
        #endregion

        #region GET&SET


        public DateTime Fecha
        {
            get => _Fecha;
            set => SetPropertyValue(nameof(Fecha), ref _Fecha, value);
        }
        public TimeSpan HoraInicio
        {
            get => _HoraInicio;
            set
            {
                if (!IsLoading)
                {
                    Inicio = _Fecha + value;

                }
                SetPropertyValue(nameof(HoraInicio), ref _HoraInicio, value);
            }
        }
        public TimeSpan HoraFin
        {
            get => _HoraFin;
            set
            {
                if(!IsLoading)
                {
                    Fin = _Fecha + value;

                }
                SetPropertyValue(nameof(HoraFin), ref _HoraFin, value);
            }
        }
        [Description("Hora inicio tarde")]
        public TimeSpan HoraInicioT
        {
            get => _HoraInicioT;
            set
            {
                if (!IsLoading)
                {
                    InicioT = _Fecha + value;

                }
                SetPropertyValue(nameof(HoraInicioT), ref _HoraInicioT, value);
            }
        }
        [Description("Hora fin tarde")]
        public TimeSpan HoraFinT
        {
            get => _HoraFinT;
            set
            {
                if (!IsLoading)
                {
                    FinT = _Fecha + value;

                }
                SetPropertyValue(nameof(HoraFinT), ref _HoraFinT, value);
            }
        }

        [Size(SizeAttribute.Unlimited)]
        public string Obsevaciones
        {
            get => _Observaciones;
            set => SetPropertyValue(nameof(Obsevaciones), ref _Observaciones, value);
        }
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public DateTime Inicio
        {
            get => _Inicio;
            set
            {
                if (!IsLoading)
                {
                    try
                    {
                        Cantidad = (_Fin - value).TotalHours + (_FinT - _Inicio).TotalHours;
                    }
                    catch
                    {
                        Cantidad = 0;
                    }
                }
                SetPropertyValue(nameof(Inicio), ref _Inicio, value); }
        }
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public DateTime Fin
        {
            get => _Fin;
            set
            {
                if(!IsLoading)
                {
                    try
                    {
                        Cantidad = (value - _Inicio).TotalHours + (_FinT - InicioT).TotalHours;
                    }
                    catch
                    {
                        Cantidad = 0;
                    }
                }
                SetPropertyValue(nameof(Fin), ref _Fin, value);
            }
        }
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public DateTime InicioT
        {
            get => _InicioT;
            set
            {
                if (!IsLoading)
                {
                    try
                    {
                        Cantidad = (_Fin - _Inicio).TotalHours+ (_FinT - value).TotalHours;
                    }
                    catch
                    {
                        Cantidad = 0;
                    }
                }
                SetPropertyValue(nameof(InicioT), ref _InicioT, value);
            }
        }
        [VisibleInListView(false)]
        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        public DateTime FinT
        {
            get => _FinT;
            set
            {
                if (!IsLoading)
                {
                    try
                    {
                        Cantidad = (_Fin - _Inicio).TotalHours + (value - _InicioT).TotalHours;
                    }
                    catch
                    {
                        Cantidad = 0;
                    }
                }
                SetPropertyValue(nameof(FinT), ref _FinT, value);
            }
        }
        public double Cantidad
        {
            get => _Cantidad;
            set => SetPropertyValue(nameof(Cantidad), ref _Cantidad, value);
        }
        public Incidencia Incidencia
        {
            get => _Incidencia;

            set
            {

                SetPropertyValue(nameof(Incidencia), ref _Incidencia, value);


            }
        }
        public ControlTurnos.Module.BusinessObjects.Temel.Employee Empleado
        {
            get => _Empleado;
            set => SetPropertyValue(nameof(Empleado), ref _Empleado, value);

        }
       
        #endregion
        #region METODOS
        public TurnosEmpleados(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            //Label = 1;
        }
        public void OnCreated()
        {

        }
        void IXafEntityObject.OnLoaded()
        {

        }

        void IXafEntityObject.OnSaving()
        {

        }
        #endregion
    }
}
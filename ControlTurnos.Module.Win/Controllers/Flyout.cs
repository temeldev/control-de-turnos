﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace ControlTurnos.Module.Win.Controllers
{
    public partial class Flyout : DevExpress.XtraEditors.XtraForm
    {
        public Flyout(string subject, DateTime start, DateTime end, string location)
        {
            InitializeComponent();
            this.labelControl1.Text = subject;
            this.labelControl2.Text = start.ToString();
            this.labelControl3.Text = end.ToString();
            this.labelControl4.Text = location;
        }
    }
}
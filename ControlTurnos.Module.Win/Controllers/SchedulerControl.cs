﻿using System;
using System.Data;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Scheduler.Win;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base.General;
using DevExpress.Xpo;
using DevExpress.XtraScheduler;

namespace ControlTurnos.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SchedulerControl : ViewController
    {
        public SchedulerControl()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            if (View.ObjectTypeInfo.Implements<IEvent>())
            {
                View.ControlsCreated -= new EventHandler(View_ControlsCreated);
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            base.OnDeactivated();
            if (View.ObjectTypeInfo.Implements<IEvent>())
            {
                View.ControlsCreated -= new EventHandler(View_ControlsCreated);
            }
        }

        private void View_ControlsCreated(object sender, EventArgs e)
        {
            ListView view = (ListView)View;
            SchedulerListEditor listEditor = (SchedulerListEditor)view.Editor;
            listEditor.SchedulerControl.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Timeline;
            listEditor.SchedulerControl.GroupType = SchedulerGroupType.Date;
            GeneraRoomChart(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, listEditor);           
        }

        private void SchedulerControl_Activated(object sender, EventArgs e)
        {

        }
        private void GeneraRoomChart(int intYEAR, int intMONTH, int intDAY, SchedulerListEditor listEditor)
        {
            SchedulerStorage schedulerStorage1 = (SchedulerStorage)listEditor.SchedulerControl.DataStorage;

            try
            {
                string strERROR = string.Empty;
                //dtAPPOINTMENTS_TurnosEmpleados = new DataTable();
                //DataTable dtRESOURCES_Empleados = new DataTable();
                //dtAPPOINTMENTS_TurnosEmpleados.TableName = "TurnosEmpleados";
                //dtRESOURCES_Empleados.TableName = "Employee";
                //DataSet dsAPPOINTMENTS_TurnosEmpleados = new DataSet();
                //DataSet dsRESOURCES_Empleados = new DataSet();
                //dsRESOURCES_Empleados.Tables.Add(dtRESOURCES_Empleados);
                //dsAPPOINTMENTS_TurnosEmpleados.Tables.Add(dtAPPOINTMENTS_TurnosEmpleados);

              //  strERROR = "Carga de Reservas";
                //Recuperamos las RESERVAS (APPOINTMENTS a nivel de Scheduler)
                XPCollection<BusinessObjects.TurnosEmpleados> turnosEmpleados = new XPCollection<BusinessObjects.TurnosEmpleados>(((XPObjectSpace)this.ObjectSpace).Session);
                XPCollection<BusinessObjects.Temel.Employee> Empleados = new XPCollection<BusinessObjects.Temel.Employee>(((XPObjectSpace)this.ObjectSpace).Session);
                //turnosEmpleados.
                //dtAPPOINTMENTS_TurnosEmpleados.Load(turnosEmpleados);
                //dsAPPOINTMENTS_RESERVAS.AcceptChanges();

                //strFiltroHabitaciones = objQuery_MC.LastQuery;
                //strFiltroHabitaciones = strFiltroHabitaciones.Replace("SELECT *", "SELECT Habitacion_RES");

                //Recuperamos las HABITACIONES (RESOURCES a nivel de Scheduler)
                //23/11/14 MV se queja de que al filtrar por fechas no salen todas las habitaciones. ACALP pues y hago esto:
                //strERROR = "Carga de Habitaciones";
                //strFiltroHabitaciones = string.Empty;
                //dtRESOURCES_HABITACIONES.Load(objAAD.recuperahabbyhot(ref objQuery_MC, strFiltroHabitaciones, strHOT, strPiso_Q, strBloque_Q, strPropietario_Q).DataViewManager.DataSet.Tables[0].CreateDataReader());

                strERROR = "Generación Room Chart";
                ResourceStorage resStorage = (ResourceStorage)schedulerStorage1.Resources;
                
                schedulerStorage1.Appointments.Mappings.Description = "Empeado";
                schedulerStorage1.Appointments.Mappings.End = "Fecha";
                schedulerStorage1.Appointments.Mappings.Label = "Incidencia"; //"NESTADO_ERERES";
                //schedulerStorage1.Appointments.Mappings.Location = "Hotel";
                schedulerStorage1.Appointments.Mappings.ResourceId = "Empleado";
                schedulerStorage1.Appointments.Mappings.Start = "Fecha";
                //schedulerStorage1.Appointments.Mappings.Status = "Agencia";
                //schedulerStorage1.Appointments.Mappings.Subject = "INFO_SUBJECT"; //CNOMBREPRIMEROCUPANTE_RES

                resStorage.Mappings.Id = "No_";
                resStorage.Mappings.Caption = "NombreCompleto";

                //resStorage
                //DataView mydatarowview;
                //DataView mydatarowview2;
                //mydatarowview = dsAPPOINTMENTS_RESERVAS.Tables[0].DefaultView;
                //mydatarowview2 = dsRESOURCES_HABITACIONES.Tables[0].DefaultView;
                resStorage.DataSource = Empleados;

                schedulerStorage1.Appointments.DataSource = turnosEmpleados;// mydatarowview;
                schedulerStorage1.Appointments.Mappings.ResourceId = "Empleado";
                schedulerStorage1.Appointments.ResourceSharing = false;
                resStorage.Mappings.Id = "No_";
                resStorage.Mappings.Caption = "NombreCompleto";

                //schRoomChart.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Timeline;

                //schRoomChart.GroupType = SchedulerGroupType.Date;

                //this.schRoomChart.Start = new DateTime(intYEAR, intMONTH, intDAY, 0, 0, 0, 0);

                //schRoomChart.OptionsView.ResourceHeaders.Height = 0;

                //dsDataSet_ReservasAFacturar = dsAPPOINTMENTS_RESERVAS.Clone();
                //dtOrigen = dtAPPOINTMENTS_RESERVAS;

                //this.ActualizaBotonesFiltro();

                strERROR = string.Empty;
            }
            catch (Exception ex)
            {
                //strERROR += "\n" + ex.Message;
                return;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Xpo;
using DevExpress.XtraScheduler;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp;
using DevExpress.Utils.Menu;
using DevExpress.XtraScheduler.Drawing;
using DevExpress.Utils.Drawing;

namespace ControlTurnos.Module.Win.Controllers
{
    public partial class DetalleTurnos : DevExpress.XtraEditors.XtraForm
    {
        IObjectSpace osObjectSpace;
        XafApplication xapAPP;
        public DetalleTurnos(IObjectSpace osObjectSpaceParam, XafApplication xapAPPParam)
        {
            osObjectSpace = osObjectSpaceParam;
            xapAPP = xapAPPParam;
            InitializeComponent();
        }
        private void frmDetalleTurnos_Load(object sender, EventArgs e)
        {
            string strERROR = string.Empty;
            try
            {
                this.Text = "Detalle Turnos";

                this.GeneraRoomChart(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

                if (strERROR != string.Empty)
                {
                    MessageBox.Show("Se ha producido un error en la carga del formulario (2)\n" + strERROR, "Detalle Turnos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // Check whether the SchedulerControl can be previewed.
                if (!this.schRoomChart.IsPrintingAvailable)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se ha producido un error en la carga del formulario\n" + ex.Message, "Detalle Turnos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void GeneraRoomChart(int intYEAR, int intMONTH, int intDAY)
        {
            
            try
            {
                string strERROR = string.Empty;
                //dtAPPOINTMENTS_TurnosEmpleados = new DataTable();
                //DataTable dtRESOURCES_Empleados = new DataTable();
                //dtAPPOINTMENTS_TurnosEmpleados.TableName = "TurnosEmpleados";
                //dtRESOURCES_Empleados.TableName = "Employee";
                //DataSet dsAPPOINTMENTS_TurnosEmpleados = new DataSet();
                //DataSet dsRESOURCES_Empleados = new DataSet();
                //dsRESOURCES_Empleados.Tables.Add(dtRESOURCES_Empleados);
                //dsAPPOINTMENTS_TurnosEmpleados.Tables.Add(dtAPPOINTMENTS_TurnosEmpleados);

                //  strERROR = "Carga de Reservas";
                //Recuperamos las RESERVAS (APPOINTMENTS a nivel de Scheduler)
                
                XPCollection<BusinessObjects.TurnosEmpleados> turnosEmpleados = new XPCollection<BusinessObjects.TurnosEmpleados>(((XPObjectSpace)this.osObjectSpace).Session);
                XPCollection<BusinessObjects.Temel.Employee> Empleados = new XPCollection<BusinessObjects.Temel.Employee>(((XPObjectSpace)this.osObjectSpace).Session);
                strERROR = "Generación Chart";
                ResourceStorage resStorage = schedulerStorage1.Resources;
                schedulerStorage1.Appointments.Mappings.Description = "Empeado.NombreCompleto";
                //MappingBase mappings = schedulerStorage1.Appointments.Mappings;
                schedulerStorage1.Appointments.Mappings.End = "Fin";
                schedulerStorage1.Appointments.Mappings.Label = "Incidencia.Oid"; //"NESTADO_ERERES";
                schedulerStorage1.Appointments.Mappings.Subject = "Incidencia.Etiqueta";
                schedulerStorage1.Appointments.Mappings.Description = "Incidencia.Descripcion";
                schedulerStorage1.Appointments.Mappings.ResourceId = "Empleado.No_";
                schedulerStorage1.Appointments.Mappings.Start = "Inicio";
                schedulerStorage1.Appointments.Mappings.Location = "Cantidad";// "Oid";
                schedulerStorage1.Appointments.Mappings.AppointmentId = "Oid";
                //schedulerStorage1.Appointments.Mappings.Subject = "INFO_SUBJECT"; //CNOMBREPRIMEROCUPANTE_RES

                resStorage.Mappings.Id = "No_";
                resStorage.Mappings.Caption = "NombreCompleto";
                //resStorage.Mappings.
                //resStorage
                //DataView mydatarowview;
                //DataView mydatarowview2;
                //mydatarowview = dsAPPOINTMENTS_RESERVAS.Tables[0].DefaultView;
                //mydatarowview2 = dsRESOURCES_HABITACIONES.Tables[0].DefaultView;
                resStorage.DataSource = Empleados;

                schedulerStorage1.Appointments.DataSource = turnosEmpleados;// mydatarowview;
                //schedulerStorage1.Appointments.Mappings.ResourceId = "Empleado.No_";
                schedulerStorage1.Appointments.ResourceSharing = false;
                //resStorage.Mappings.Id = "No_";
                //resStorage.Mappings.Caption = "NombreCompleto";

                schRoomChart.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Timeline;

                schRoomChart.GroupType = SchedulerGroupType.Date;

                this.schRoomChart.Start = new DateTime(intYEAR, intMONTH, intDAY, 0, 0, 0, 0);

                schRoomChart.OptionsView.ResourceHeaders.Height = 0;
                schRoomChart.Views.TimelineView.ResourcesPerPage = 20;

                //dsDataSet_ReservasAFacturar = dsAPPOINTMENTS_RESERVAS.Clone();
                //dtOrigen = dtAPPOINTMENTS_RESERVAS;

                //this.ActualizaBotonesFiltro();

                strERROR = string.Empty;
            }
            catch (Exception ex)
            {
                //strERROR += "\n" + ex.Message;
                return;
            }
        }
        private void SchRoomChart_AppointmentViewInfoCustomizing(object sender, AppointmentViewInfoCustomizingEventArgs e)
        {
            if (e.ViewInfo.DisplayText == String.Empty)
                e.ViewInfo.ToolTipText = String.Format("Started at {0:g}", e.ViewInfo.Appointment.Start);
            if (e.ViewInfo.DisplayText!= String.Empty)
            {
                e.ViewInfo.Options.ShowEndTime = false;
                e.ViewInfo.Options.ShowStartTime = false;
                
            }
        }
        private void schRoomChart_PopupMenuShowing(object sender, DevExpress.XtraScheduler.PopupMenuShowingEventArgs e)
        {
            int intItems = 0;
            int intIndex = -1;
            string strRES = string.Empty;
            int intEstado = -1;

            try
            {
                //Número de elementos del menú actual
                intItems = e.Menu.Items.Count;

                //Eliminamos el contenido del Menú
                for (int intI = 0; intI < intItems; intI++)
                {
                    e.Menu.Items.Remove(e.Menu.Items[0]);
                }

                switch (e.Menu.Id.ToString().ToLower())
                {
                    case "defaultmenu": //Nueva Reserva
                        //Añadimos NUEVA RESERVA
                        intIndex = this.AgregarMenuItem(e, this.CrearMenuItem("Añadir Nueva Incidencia", true, Nueva_Click));
                        break;
                    case "appointmentmenu": //Sobre una Reserva
                        //Recuperamos la Reserva sobre la que estamos
                        if (this.schRoomChart.SelectedAppointments.Count > 0)
                        {
                            strRES = this.schRoomChart.SelectedAppointments[0].GetValue(schedulerStorage1, "Empleado.No_").ToString();
                           // string strEstado =(this.schRoomChart.SelectedAppointments[0].GetValue(schedulerStorage1, "Incidencia.Descripcion").ToString());

                            //if (strRES == "0")
                            //{
                            //    return;
                            //}
                        }

                        #region CREAR MENÚ CONTEXTUAL RESERVAS EXISTENTES
                        //Añadimos FICHA
                        intIndex = this.AgregarMenuItem(e, this.CrearMenuItem("Ficha", (strRES != string.Empty), Ficha_Click));
                        break;
                        #endregion CREAR MENÚ CONTEXTUAL RESERVAS EXISTENTES
                }
            }
            catch (Exception ex)
            {
            }
        }
        private int AgregarMenuItem(DevExpress.XtraScheduler.PopupMenuShowingEventArgs e, DXMenuItem mitItem)
        {
            int intIndex = -1;

            intIndex = e.Menu.Items.Add(mitItem);

            return intIndex;
        }
        private DXMenuItem CrearMenuItem(string strCaption, bool blnEnabled, EventHandler evhClick)
        {
            DevExpress.Utils.Menu.DXMenuItem mitItem = new DevExpress.Utils.Menu.DXMenuItem();
            mitItem.Caption = strCaption;
            mitItem.Enabled = blnEnabled;
            if (evhClick != null)
                mitItem.Click += new EventHandler(evhClick);

            return mitItem;
        }
        private void Nueva_Click(object sender, System.EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            //this.NuevaReserva(); //OJO DESCOMENTAR!!!!

            this.Cursor = Cursors.Default;
        }
        private void Ficha_Click(object sender, System.EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            if (this.schRoomChart.SelectedAppointments.Count == 1)
            {
                this.EditarDia(Int32.Parse(this.schRoomChart.SelectedAppointments[0].Id.ToString()));
            }
            else
                MessageBox.Show("Seleccione una Trabajador", "Detalle Turnos", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            this.Cursor = Cursors.Default;
        }
        private void EditarDia(int Oid)
        {
           
            string strERROR = string.Empty;

            try
            {
                IObjectSpace osDV = xapAPP.CreateObjectSpace();

                BusinessObjects.TurnosEmpleados objRES_DV = osDV.GetObjectByKey<BusinessObjects.TurnosEmpleados>(Oid);

                DetailView dv = xapAPP.CreateDetailView(osDV, objRES_DV);
                ShowViewParameters svp = new ShowViewParameters(dv);
                svp.TargetWindow = TargetWindow.NewModalWindow;
                xapAPP.ShowViewStrategy.ShowView(svp, new ShowViewSource(null, null));

               // objReserva = objRES_DV;

                DateTime dtmInicio = schRoomChart.Start;
                this.GeneraRoomChart(dtmInicio.Year, dtmInicio.Month, dtmInicio.Day);
            }
            catch (Exception ex)
            { }
        }
        private void schRoomChart_AppointmentDrop(object sender, AppointmentDragEventArgs e)
        {
            //#region Declaración de Variables
            //string strRES_Edit = string.Empty;
            //string strHAB_Edit = string.Empty;
            //int intHAB_Edit = -1;
            //string strHAB_Edit_DESC = string.Empty;
            //string strHAB_ACTUAL = string.Empty;
            //string strHAB_ACTUAL_DESC = string.Empty;
            //int intHAB_ACTUAL = -1;



            //DateTime dtmEntrada_Edit = new DateTime();
            //DateTime dtmSalida_Edit = new DateTime();
            //int intEstado_Edit = -1;
            //int intAGE_EDIT = -1;
            //int intCON_Edit = -1;
            //int intREG_FAC_Edit = -1;
            //int intTHA_FAC_Edit = -1;
            //int intTHA_Edit = -1;
            //string strError = string.Empty;
            //string strPregunta = string.Empty;
            //#endregion

            //this.Cursor = Cursors.WaitCursor;

            //try
            //{
            //    BusinessObjects.Configuracion.Maestros.Habitacion objHAB_AUX;

            //    //Recuperamos la información de la Reserva
            //    if (!this.Captura_Reserva(0))
            //    {
            //        //e.Handled = true; no funciona en 15.9
            //        e.Allow = false;
            //        this.Cursor = Cursors.Default;
            //        return;
            //    }

            //    //Recuperamos el Id de la Reserva
            //    strRES_Edit = e.SourceAppointment.GetValue(schedulerStorage1, "ID_RESERVA").ToString();
            //    //también podría ser: strRES_Edit = e.EditedAppointment.GetValue(schedulerStorage1, "Habitacion_RES").ToString();

            //    if (strRES_Edit == "0")
            //    {
            //        e.Allow = false;
            //        this.Cursor = Cursors.Default;
            //        return;
            //    }

            //    //Recuperamos la fecha de Entrada
            //    strError = "entrada";
            //    dtmEntrada_Edit = e.EditedAppointment.Start;
            //    //Recuperamos la fecha de Salida
            //    strError = "salida";
            //    dtmSalida_Edit = e.EditedAppointment.End;

            //    TimeSpan tmsTrabajo = dtmEntrada_Edit - ObjParametrosRutaSoftware.FECHA_DE_TRABAJO;
            //    int intTrabajo = int.Parse(tmsTrabajo.TotalDays.ToString());

            //    //Si la fecha de trabajo es posterior a la de entrada propuesta, no dejo seguir
            //    if (intTrabajo < 0)
            //    {
            //        //e.Handled = true; No funciona en 15.9
            //        e.Allow = false;
            //        this.Cursor = Cursors.Default;
            //        return;
            //    }

            //    //Recuperamos la Habitación
            //    strHAB_Edit = e.HitResource.Id.ToString();
            //    intHAB_Edit = int.Parse(e.HitResource.Id.ToString());
            //    strHAB_ACTUAL = this.objReserva.Habitacion.Oid.ToString();
            //    strHAB_ACTUAL_DESC = this.objReserva.Habitacion.ID;
            //    intHAB_ACTUAL = this.objReserva.Habitacion.Oid;

            //    if (strHAB_Edit.Trim() == string.Empty || strHAB_ACTUAL == string.Empty)
            //    {
            //        //e.Handled = true; No funciona en 15.9
            //        e.Allow = false;
            //        this.Cursor = Cursors.Default;
            //        return;
            //    }

            //    if (this.objReserva.Entrada != dtmEntrada_Edit)
            //    {
            //        if (strHAB_ACTUAL.Trim() != strHAB_Edit.Trim())
            //        {
            //            //objReserva.Habitacion = objReserva.Session.GetObjectByKey<BusinessObjects.Configuracion.Maestros.Habitacion>(intHAB_Edit);
            //            objHAB_AUX = objReserva.Session.GetObjectByKey<BusinessObjects.Configuracion.Maestros.Habitacion>(intHAB_Edit);
            //            //strHAB_Edit_DESC = objReserva.Habitacion.ID;
            //            strHAB_Edit_DESC = objHAB_AUX.ID;
            //            strPregunta = string.Format("¿Cambiar la Fecha de Entrada de la Reserva {0} al día {1} y pasarla a la Habitación {2}?", strRES_Edit, dtmEntrada_Edit.ToShortDateString(), strHAB_Edit_DESC);

            //            //Tipo Habitación Uso
            //            //this.objReserva.TipoHabitacionUso = objReserva.Session.GetObjectByKey<BusinessObjects.Configuracion.Maestros.TipoHabitacion>(objReserva.Habitacion.TipoHabitacion.Oid);
            //        }
            //        else
            //            strPregunta = string.Format("¿Cambiar la Fecha de Entrada de la Reserva {0} al día {1}?", strRES_Edit, dtmEntrada_Edit.ToShortDateString());
            //    }
            //    else
            //    {
            //        if (strHAB_ACTUAL.Trim() != strHAB_Edit.Trim())
            //        {
            //            //objReserva.Habitacion = objReserva.Session.GetObjectByKey<BusinessObjects.Configuracion.Maestros.Habitacion>(intHAB_Edit);
            //            objHAB_AUX = objReserva.Session.GetObjectByKey<BusinessObjects.Configuracion.Maestros.Habitacion>(intHAB_Edit);
            //            //strHAB_Edit_DESC = objReserva.Habitacion.ID;
            //            strHAB_Edit_DESC = objHAB_AUX.ID;
            //            strPregunta = string.Format("¿Asignar la Habitación {0} a la Reserva {1}?", strHAB_Edit_DESC, strRES_Edit);
            //            //objReserva.Habitacion = objReserva.Session.GetObjectByKey<BusinessObjects.Configuracion.Maestros.Habitacion>(intHAB_Edit);
            //            //Tipo Habitación Uso
            //            //this.objReserva.TipoHabitacionUso = objReserva.Session.GetObjectByKey<BusinessObjects.Configuracion.Maestros.TipoHabitacion>(objReserva.Habitacion.TipoHabitacion.Oid);
            //        }
            //        else
            //        {
            //            //e.Handled = true; No funciona en 15.9
            //            e.Allow = false;
            //            this.Cursor = Cursors.Default;
            //            return;
            //        }
            //    }

            //    if (intTHA_Edit == -1)
            //        intTHA_Edit = this.Recupera_THA_Habitacion(intHAB_Edit, LibreriaObjetosRuta.ObjParametrosRutaSoftware.HOTEL_DE_TRABAJO_CODIGO);

            //    if (MessageBox.Show(strPregunta, strCaptionUSC, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            //    {
            //        //e.Handled = true; No funciona en 15.9
            //        e.Allow = false;
            //        this.Cursor = Cursors.Default;
            //        return;
            //    }

            //    //Recuperamos el Estado
            //    intEstado_Edit = int.Parse(e.SourceAppointment.GetValue(schedulerStorage1, "EstadoReserva").ToString());
            //    intEstado_Edit = this.objReserva.EstadoReserva.Oid;
            //    //Recuperamos la Agencia
            //    intAGE_EDIT = this.objReserva.Agencia.Oid;
            //    //Recuperamos el Contrato
            //    intCON_Edit = this.objReserva.Contrato.Oid;
            //    //Recuperamos el Régimen Factura
            //    intREG_FAC_Edit = this.objReserva.RegimenFactura.Oid;
            //    //Recuperamos el Tipo de Habitación Factura
            //    intTHA_FAC_Edit = this.objReserva.TipoHabitacionFactura.Oid;

            //    //Validamos las Fechas
            //    if (!this.ValidaFechas(dtmEntrada_Edit, dtmSalida_Edit, intEstado_Edit))
            //    {
            //        //e.Handled = true; No funciona en 15.9
            //        e.Allow = false;
            //        this.Cursor = Cursors.Default;
            //        return;
            //    }

            //    ////Validamos el Bloqueo de Ventas
            //    //if (!this.ValidaBloqueoVentas(intAGE_EDIT, intEstado_Edit, dtmEntrada_Edit, dtmSalida_Edit))
            //    //{
            //    //    //e.Handled = true; No funciona en 15.9
            //    //    e.Allow = false;
            //    //    this.Cursor = Cursors.Default;
            //    //    return;
            //    //}

            //    if (!this.ValidaPreciosReserva(dtmEntrada_Edit, dtmSalida_Edit, intCON_Edit, this.intHotelTrabajo, intREG_FAC_Edit, intTHA_FAC_Edit))
            //    {
            //        if (MessageBox.Show("Existen días que no tienen Precios asignados para la combinación Agencia/Contrato/Régimen/Tipo Habitación.\n¿Continuar de todos modos?", strCaptionUSC, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
            //        {
            //            //e.Handled = true; No funciona en 15.9

            //            e.Allow = false;
            //            this.Cursor = Cursors.Default;
            //            return;
            //        }
            //    }

            //    this.ActualizarReserva(dtmEntrada_Edit, dtmSalida_Edit, intHAB_Edit, intTHA_Edit);
            //}
            //catch (Exception ex)
            //{
            //    switch (strError)
            //    {
            //        case "entrada":
            //            MessageBox.Show("El valor de la Fecha de Entrada no es correcto", strCaptionUSC, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //            break;
            //        case "salida":
            //            MessageBox.Show("El valor de la Fecha de Salida no es correcto", strCaptionUSC, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //            break;
            //        default:
            //            MessageBox.Show("Se ha producido un error:\n" + ex.Message, strCaptionUSC, MessageBoxButtons.OK, MessageBoxIcon.Error);
            //            break;
            //    }
            //}

            //this.Cursor = Cursors.Default;
        }

        private void schRoomChart_AppointmentResized(object sender, AppointmentResizeEventArgs e)
        {
            //#region Declaración de Variables
            //string strRES_Edit = string.Empty;
            //string strHAB_Edit = string.Empty;
            //int intHAB_Edit = -1;
            //string strHAB_Edit_DESC = string.Empty;
            //string strHAB_ACTUAL = string.Empty;
            //string strHAB_ACTUAL_DESC = string.Empty;
            //int intHAB_ACTUAL = -1;

            //DateTime dtmEntrada_Edit = new DateTime();
            //DateTime dtmSalida_Edit = new DateTime();
            //int intEstado_Edit = -1;
            //int intAGE_EDIT = -1;
            //int intCON_Edit = -1;
            //int intREG_FAC_Edit = -1;
            //int intTHA_FAC_Edit = -1;
            //int intTHA_Uso_Edit = -1;
            //string strError = string.Empty;
            //string strPregunta = string.Empty;
            //string strERROR = string.Empty;
            //#endregion

            //try
            //{
            //    this.Cursor = Cursors.WaitCursor;

            //    TimeSpan tmsTrabajo;
            //    int intTrabajo = 0;

            //    //Recuperamos la información de la Reserva
            //    if (!this.Captura_Reserva(0))
            //    {
            //        this.Cursor = Cursors.Default;
            //        return;
            //    }

            //    strRES_Edit = e.SourceAppointment.GetValue(schedulerStorage1, "ID_RESERVA").ToString();
            //    //también podría ser: strRES_Edit = e.EditedAppointment.GetValue(schedulerStorage1, "Habitacion_RES").ToString();

            //    if (strRES_Edit == "0")
            //    {
            //        e.Handled = true;
            //        e.Allow = false;
            //        this.Cursor = Cursors.Default;
            //        return;
            //    }

            //    //Recuperamos la fecha de Entrada
            //    strError = "entrada";
            //    dtmEntrada_Edit = e.EditedAppointment.Start;
            //    //Recuperamos la fecha de Salida
            //    strError = "salida";
            //    dtmSalida_Edit = e.EditedAppointment.End;

            //    if (this.objReserva.EstadoReserva.Oid == 3)
            //    {
            //        //Si el estado es ENTRADA, sólo puede modificar la fecha de salida
            //        if (e.ResizedSide == ResizedSide.AtStartTime)
            //        {
            //            e.Handled = true;
            //            e.Allow = false;

            //            this.Cursor = Cursors.Default;
            //            return;
            //        }
            //        else
            //        {
            //            tmsTrabajo = dtmSalida_Edit - LibreriaObjetosRuta.ObjParametrosRutaSoftware.FECHA_DE_TRABAJO;
            //            intTrabajo = int.Parse(tmsTrabajo.TotalDays.ToString());

            //            //Si la fecha de trabajo es posterior a la de salida propuesta, no dejo seguir
            //            if (intTrabajo < 0)
            //            {
            //                e.Handled = true;
            //                e.Allow = false;
            //                this.Cursor = Cursors.Default;
            //                return;
            //            }
            //        }
            //    }
            //    else if (this.objReserva.EstadoReserva.Oid != 2)
            //    {
            //        e.Handled = true;
            //        e.Allow = false;

            //        this.Cursor = Cursors.Default;
            //        return;
            //    }
            //    else
            //    {
            //        //estado 2

            //        tmsTrabajo = dtmEntrada_Edit - LibreriaObjetosRuta.ObjParametrosRutaSoftware.FECHA_DE_TRABAJO;
            //        intTrabajo = int.Parse(tmsTrabajo.TotalDays.ToString());

            //        //Si la fecha de trabajo es posterior a la de entrada propuesta, no dejo seguir
            //        if (intTrabajo < 0)
            //        {
            //            e.Handled = true;
            //            e.Allow = false;
            //            this.Cursor = Cursors.Default;
            //            return;
            //        }
            //    }

            //    //Recuperamos el Id de la Reserva
            //    strRES_Edit = objReserva.Oid.ToString(); //e.SourceAppointment.GetValue(schedulerStorage1, "Habitacion_RES").ToString();
            //    //también podría ser: strRES_Edit = e.EditedAppointment.GetValue(schedulerStorage1, "Habitacion_RES").ToString();

            //    ////Recuperamos la fecha de Entrada
            //    //strError = "entrada";
            //    //dtmEntrada_Edit = e.EditedAppointment.Start;
            //    ////Recuperamos la fecha de Salida
            //    //strError = "salida";
            //    //dtmSalida_Edit = e.EditedAppointment.End;

            //    //TimeSpan tmsTrabajo = dtmEntrada_Edit - LibreriaObjetosRuta.ObjParametrosRutaSoftware.FECHA_DE_TRABAJO;
            //    //int intTrabajo = int.Parse(tmsTrabajo.TotalDays.ToString());

            //    //Si la fecha de trabajo es posterior a la de entrada propuesta, no dejo seguir
            //    //if (intTrabajo < 0)
            //    //{
            //    //    e.Handled = true;
            //    //    e.Allow = false;
            //    //    this.Cursor = Cursors.Default;
            //    //    return;
            //    //}

            //    //Recuperamos la Habitación
            //    strHAB_Edit = this.objReserva.Habitacion.ID;
            //    intHAB_Edit = this.objReserva.Habitacion.Oid;
            //    strHAB_ACTUAL = this.objReserva.Habitacion.ID;
            //    intHAB_ACTUAL = this.objReserva.Habitacion.Oid;

            //    if (strHAB_Edit.Trim() == string.Empty || strHAB_ACTUAL == string.Empty)
            //    {
            //        e.Handled = true;
            //        e.Allow = false;
            //        this.Cursor = Cursors.Default;
            //        return;
            //    }

            //    if (this.objReserva.Entrada != dtmEntrada_Edit)
            //        strPregunta = string.Format("¿Cambiar la Fecha de Entrada de la Reserva {0} al día {1}?", strRES_Edit, dtmEntrada_Edit.ToShortDateString());
            //    else if (this.objReserva.Salida != dtmSalida_Edit)
            //        strPregunta = string.Format("¿Cambiar la Fecha de Salida de la Reserva {0} al día {1}?", strRES_Edit, dtmSalida_Edit.ToShortDateString());
            //    else
            //    {
            //        e.Handled = true;
            //        e.Allow = false;
            //        this.Cursor = Cursors.Default;
            //        return;
            //    }

            //    if (MessageBox.Show(strPregunta, strCaptionUSC, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            //    {
            //        //e.Handled = true;
            //        DateTime dtmInicio = schRoomChart.Start;
            //        //this.schRoomChart.Start = new DateTime(dtmInicio.Year, dtmInicio.Month, dtmInicio.Day, 0, 0, 0, 0);
            //        //this.GeneraRoomChart(dtmInicio.Year, dtmInicio.Month, dtmInicio.Day);

            //        BeginInvoke(
            //                    new Action<SchedulerControl, Appointment>
            //                        (delegate
            //                            (SchedulerControl schdCtrl, Appointment appt)
            //                        {
            //                            this.GeneraRoomChart(dtmInicio.Year, dtmInicio.Month, dtmInicio.Day, ref strERROR);
            //                        }
            //                        )
            //                        , schRoomChart, e.SourceAppointment);

            //        e.Handled = true;
            //        this.Cursor = Cursors.Default;
            //        return;
            //    }

            //    //Recuperamos el Tipo de Habitación Uso
            //    intTHA_Uso_Edit = this.objReserva.TipoHabitacionUso.Oid;
            //    //Recuperamos el Estado
            //    intEstado_Edit = int.Parse(e.SourceAppointment.GetValue(schedulerStorage1, "EstadoReserva").ToString());
            //    intEstado_Edit = this.objReserva.EstadoReserva.Oid;
            //    //Recuperamos la Agencia
            //    intAGE_EDIT = this.objReserva.Agencia.Oid;
            //    //Recuperamos el Contrato
            //    intCON_Edit = this.objReserva.Contrato.Oid;
            //    //Recuperamos el Régimen Factura
            //    intREG_FAC_Edit = this.objReserva.RegimenFactura.Oid;
            //    //Recuperamos el Tipo de Habitación Factura
            //    intTHA_FAC_Edit = this.objReserva.TipoHabitacionFactura.Oid;

            //    //Validamos las Fechas
            //    if (!this.ValidaFechas(dtmEntrada_Edit, dtmSalida_Edit, intEstado_Edit))
            //    {
            //        e.Handled = true;
            //        e.Allow = false;
            //        this.Cursor = Cursors.Default;
            //        return;
            //    }

            //    ////Validamos el Bloqueo de Ventas
            //    //if (!this.ValidaBloqueoVentas(intAGE_EDIT, intEstado_Edit, dtmEntrada_Edit, dtmSalida_Edit))
            //    //{
            //    //    e.Handled = true;
            //    //    e.Allow = false;
            //    //    this.Cursor = Cursors.Default;
            //    //    return;
            //    //}

            //    if (!this.ValidaPreciosReserva(dtmEntrada_Edit, dtmSalida_Edit, intCON_Edit, this.intHotelTrabajo, intREG_FAC_Edit, intTHA_FAC_Edit))
            //    {
            //        if (MessageBox.Show("Existen días que no tienen Precios asignados para la combinación Agencia/Contrato/Régimen/Tipo Habitación.\n¿Continuar de todos modos?", strCaptionUSC, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
            //        {
            //            e.Handled = true;
            //            e.Allow = false;
            //            this.Cursor = Cursors.Default;
            //            return;
            //        }
            //    }

            //    this.ActualizarReserva(dtmEntrada_Edit, dtmSalida_Edit, intHAB_Edit, intTHA_Uso_Edit);
            //}
            //catch (Exception ex)
            //{
            //    switch (strError)
            //    {
            //        case "entrada":
            //            MessageBox.Show("El valor de la Fecha de Entrada no es correcto", strCaptionUSC, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //            break;
            //        case "salida":
            //            MessageBox.Show("El valor de la Fecha de Salida no es correcto", strCaptionUSC, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //            break;
            //        default:
            //            MessageBox.Show("Se ha producido un error:\n" + ex.Message, strCaptionUSC, MessageBoxButtons.OK, MessageBoxIcon.Error);
            //            //objETX.ErrorToXML(this.strCaptionUSC, DateTime.Now, ObjParametrosRutaSoftware.FECHA_DE_TRABAJO, this.objParametrosRUTA.CADENA_HOTEL, LibreriaObjetosRuta.ObjParametrosRutaSoftware.HOTEL_DE_TRABAJO_CODIGO, this.objParametrosRUTA.USUARIO_APLICACION, "grdView_ValidateRow", -1, ex.Message, ex.GetType().Name, ex.Source, ex.TargetSite.Name, false, true, this.objParametrosRUTA.CADENA_DE_CONEXION);
            //            break;
            //    }
            //}

            //this.Cursor = Cursors.Default;
        }

        private void btnFiltrar_Edit_Click(object sender, EventArgs e)
        {
            
        }
        private void ActualizaBotonesFiltro()
        {
            bool blnHayFiltro = false;
            bool blnHayFiltroAplicado = false;

            blnHayFiltro = this.HayFiltro();
            blnHayFiltroAplicado = this.HayFiltroAplicado();

            this.btnFiltrar.Enabled = blnHayFiltro && !blnHayFiltroAplicado;
            this.btnFiltrar_Edit.Enabled = true;
            this.btnReset.Enabled = blnHayFiltro && blnHayFiltroAplicado;

            this.btnFiltrar.ToolTip = this.btnFiltrar.Enabled ? "Aplicar Filtro" + this.TooTipHayFiltro() : string.Empty;
            this.btnReset.ToolTip = this.btnReset.Enabled ? "No Aplicar Filtro" + this.TooTipHayFiltro() : string.Empty;
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            string strERROR = string.Empty;
            this.Cursor = Cursors.WaitCursor;

            //Recuperamos valores
          
            DateTime dtmInicio = schRoomChart.Start;
            this.GeneraRoomChart(dtmInicio.Year, dtmInicio.Month, dtmInicio.Day);

            this.Cursor = Cursors.Default;
        }

        private bool HayFiltro()
        {
            //if (strAGE_Q_FRM.Trim() != "0")
            //    return true;

            //if (strTHA_Q_FRM.Trim() != "0")
            //    return true;

            //if (strERE_Q_FRM.Trim() != "0")
            //    return true;

            //if (strBloque_Q_FRM.Trim() != "0")
            //    return true;

            //if (strPiso_Q_FRM.Trim() != "0")
            //    return true;

            //if (strPropietario_Q_FRM.Trim() != "0")
            //    return true;

            ////if (strEntrada_Q_FRM.Trim() != "0" && strSalida_Q_FRM.Trim() != "0")
            //if (strEntrada_Q_FRM.Trim() != "0")
            //    return true;

            return false;
        }

        private string TooTipHayFiltro()
        {
            string strToolTipFiltro = string.Empty;
            //string strSALTO = "\n";

            ////if (strEntrada_Q_DESC.Trim() != "" && strSalida_Q_DESC.Trim() != "")
            //if (strEntrada_Q_DESC.Trim() != string.Empty)
            //    strToolTipFiltro += strSALTO + "Fecha de Referencia: " + strEntrada_Q_DESC;

            ////strToolTipFiltro += strSALTO + "Fecha de Entrada entre: " + strEntrada_Q_DESC + " y " + strSalida_Q_DESC;

            //if (strAGE_Q_DESC.Trim() != string.Empty)
            //    strToolTipFiltro += strSALTO + "Agencia: " + strAGE_Q_DESC;

            //if (strERE_Q_DESC.Trim() != string.Empty)
            //    strToolTipFiltro += strSALTO + "Estado: " + strERE_Q_DESC;

            //if (strBloque_Q_DESC.Trim() != string.Empty)
            //    strToolTipFiltro += strSALTO + "Complejo: " + strBloque_Q_DESC;

            //if (strPiso_Q_DESC.Trim() != string.Empty)
            //    strToolTipFiltro += strSALTO + "Zona: " + strPiso_Q_DESC;

            //if (strPropietario_Q_DESC.Trim() != string.Empty)
            //    strToolTipFiltro += strSALTO + "Propietario: " + strPropietario_Q_DESC;

            return strToolTipFiltro;
        }

        private bool HayFiltroAplicado()
        {
            //if (strAGE_Q.Trim() != "0")
            //    return true;

            //if (strTHA_Q.Trim() != "0")
            //    return true;

            ////if (strEntrada_Q.Trim() != "0" && strSalida_Q.Trim() != "0")
            //if (strEntrada_Q.Trim() != "0")
            //    return true;

            //if (strERE_Q.Trim() != "0")
            //    return true;

            //if (strBloque_Q.Trim() != "0")
            //    return true;

            //if (strPiso_Q.Trim() != "0")
            //    return true;

            //if (strPropietario_Q.Trim() != "0")
            //    return true;

            return false;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            string strERROR = string.Empty;

            this.Cursor = Cursors.WaitCursor;

            DateTime dtmInicio = schRoomChart.Start;
            this.GeneraRoomChart(dtmInicio.Year, dtmInicio.Month, dtmInicio.Day);

            this.Cursor = Cursors.Default;
        }

        private void schRoomChart_MouseMove(object sender, MouseEventArgs e)
        {
            string strRES_Edit = string.Empty;

            try
            {
                Point pos = new Point(e.X, e.Y);
                SchedulerViewInfoBase viewInfo = schRoomChart.ActiveView.ViewInfo;
                SchedulerHitInfo hitInfo = viewInfo.CalcHitInfo(pos, false);

                if (hitInfo.HitTest == SchedulerHitTest.AppointmentContent)
                {
                    Appointment apt = ((AppointmentViewInfo)hitInfo.ViewInfo).Appointment;
                    //Text = apt.Subject;

                    //Recuperamos el Id de la Reserva
                    // strRES_Edit = apt.GetValue(schedulerStorage1, "OID").ToString(); //e.SourceAppointment.GetValue(schedulerStorage1, "ID_RESERVA").ToString();
                    int Oid = Convert.ToInt16(apt.Id.ToString());
                    //también podría ser: strRES_Edit = e.EditedAppointment.GetValue(schedulerStorage1, "ID_RESERVA").ToString();
                    BusinessObjects.TurnosEmpleados objRES_DV = osObjectSpace.GetObjectByKey<BusinessObjects.TurnosEmpleados>(Oid);
                    SchedulerHitTest o = hitInfo.HitTest;

                    //ttcRoomChart.ActiveObjectInfo.Text = "hola que tal"; // this.RecuperaInfoToolTipRES(int.Parse(strRES_Edit));
                }
            }
            catch (Exception ex)
            {
            }
        }
        private void SchRoomChart_CustomDrawAppointment(object sender, DevExpress.XtraScheduler.CustomDrawObjectEventArgs e)
            {
                //throw new System.NotImplementedException();
            //    AppointmentViewInfo viewInfo = e.ObjectInfo as AppointmentViewInfo;
            //    // Get DevExpress images. 
            //    Image im = DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png");

            //    Rectangle imageBounds = new Rectangle(viewInfo.InnerBounds.X, viewInfo.InnerBounds.Y, im.Width, im.Height);
            //    Rectangle mainContentBounds = new Rectangle(viewInfo.InnerBounds.X, viewInfo.InnerBounds.Y + im.Width + 1,
            //        viewInfo.InnerBounds.Width, viewInfo.InnerBounds.Height - im.Height - 1);
            //// Draw image in the appointment. 
            ////e.Cache.Graphics.DrawImage(im, imageBounds);

            //int statusDelta = 0;
            //for (int i = 0; i < viewInfo.StatusItems.Count; i++)
            //{
            //    AppointmentViewInfoStatusItem statusItem = viewInfo.StatusItems[i] as AppointmentViewInfoStatusItem;
            //    // Fill the status bar. 
            //    e.Cache.FillRectangle(statusItem.BackgroundViewInfo.Brush, statusItem.BackgroundViewInfo.Bounds);
            //    e.Cache.FillRectangle(statusItem.ForegroundViewInfo.Brush, statusItem.ForegroundViewInfo.Bounds);
            //    // Draw the status bar rectangle. 
            //    e.Cache.DrawRectangle(new Pen(statusItem.ForegroundViewInfo.BorderColor), statusItem.BackgroundViewInfo.Bounds);
            //    e.Cache.DrawRectangle(new Pen(statusItem.ForegroundViewInfo.BorderColor), statusItem.ForegroundViewInfo.Bounds);
            //    statusDelta = Math.Max(statusDelta, statusItem.Bounds.Width);
            //}
            //// Draw the appointment caption text. 
            //e.Cache.DrawString(viewInfo.DisplayText.Trim(), viewInfo.Appearance.Font,
            //        viewInfo.Appearance.GetForeBrush(e.Cache), mainContentBounds, StringFormat.GenericDefault);
            //    SizeF subjSize = e.Graphics.MeasureString(viewInfo.DisplayText.Trim(), viewInfo.Appearance.Font, mainContentBounds.Width);
            //    int lineYposition = (int)subjSize.Height;

            //    Rectangle descriptionLocation = new Rectangle(mainContentBounds.X, mainContentBounds.Y + lineYposition,
            //        mainContentBounds.Width, mainContentBounds.Height - lineYposition);
            //    if (viewInfo.Appointment.Description.Trim() != "")
            //    {
            //        // Draw the line above the appointment description. 
            //        e.Cache.Graphics.DrawLine(viewInfo.Appearance.GetForePen(e.Cache), descriptionLocation.Location,
            //            new Point(descriptionLocation.X + descriptionLocation.Width, descriptionLocation.Y));
            //        // Draw the appointment description text. 
            //        e.Cache.DrawString(viewInfo.Appointment.Description.Trim(), viewInfo.Appearance.Font,
            //            viewInfo.Appearance.GetForeBrush(e.Cache), descriptionLocation, StringFormat.GenericTypographic);
            //    }
           //     e.Handled = true;
            }


        private void SchRoomChart_CustomizeAppointmentFlyout(object sender, DevExpress.XtraScheduler.CustomizeAppointmentFlyoutEventArgs e)
        {
            e.ShowReminder = false;
            
        }
        public static void SchRoomChart_CustomDrawAppointmentFlyoutSubject(object sender, CustomDrawAppointmentFlyoutSubjectEventArgs e)
        {
            e.Handled = true;
            CustomDrawAppointmentFlyoutSubject(e);
        }
        static void CustomDrawAppointmentFlyoutSubject(CustomDrawAppointmentFlyoutSubjectEventArgs e)
        {
            AppointmentBandDrawerViewInfoBase viewInfo = (AppointmentBandDrawerViewInfoBase)e.ObjectInfo;
            e.DrawBackgroundDefault();
            CustomDrawAppointmentFlyoutSubject(e.Appointment, viewInfo);
        }
        private void SchRoomChart_AppointmentFlyoutShowing(object sender, DevExpress.XtraScheduler.AppointmentFlyoutShowingEventArgs e)
        {
            e.Control = new Flyout(e.FlyoutData.Subject, e.FlyoutData.Start, e.FlyoutData.End, e.FlyoutData.Location);
        }
        static void CustomDrawAppointmentFlyoutSubject(Appointment appointment, AppointmentBandDrawerViewInfoBase viewInfo)
        {
            GraphicsCache cache = viewInfo.Cache;
            StringFormat stringFormat = new StringFormat(viewInfo.View.Appearance.GetStringFormat());
            stringFormat.Alignment = stringFormat.LineAlignment = StringAlignment.Center;
            try
            {
                // Draw status
                Rectangle statusRect = GetStatusBounds(viewInfo);
                cache.FillRectangle(viewInfo.View.Status.GetBrush(), statusRect);

                if (viewInfo.View.Status.Type == AppointmentStatusType.Free)
                {
                    // Draw a warning
                   // cache.DrawImage(GetWarningIcon(new Size(statusRect.Height, statusRect.Height)), statusRect.Location);
                   // cache.DrawString("Status is unacceptable", fontStorage.StatusFont, Brushes.Red, statusRect, stringFormat);
                }
                // Draw subject  
                Font fnt1 = new Font("Verdana", 12f);
                cache.DrawString(appointment.Subject, fnt1, Brushes.Black, GetSubjectBounds(viewInfo), stringFormat);
            }
            finally
            {
                stringFormat.Dispose();
            }

        }
        static Rectangle GetSubjectBounds(AppointmentBandDrawerViewInfoBase viewInfo)
        {
            Rectangle bounds = viewInfo.Bounds;
            if (viewInfo.View.Status.Type == AppointmentStatusType.Free)
            {
                bounds.Offset(0, 10);
            }
            return bounds;
        }
        static Rectangle GetStatusBounds(AppointmentBandDrawerViewInfoBase viewInfo)
        {
            Rectangle bounds = Rectangle.Inflate(viewInfo.Bounds, -1, -1);
            if (viewInfo.View.Status.Type == AppointmentStatusType.Free)
            {
                bounds.Height = 20;
            }
            else
            {
                bounds.Height = 5;
            }
            return bounds;
        }

        private string RecuperaInfoToolTipRES(int Oid)
        {
            
            
            string strTOOL_TIPO_RES = string.Empty;
            string strSALTO = "\n";

            try
            {
                BusinessObjects.TurnosEmpleados Tur= osObjectSpace.GetObjectByKey<BusinessObjects.TurnosEmpleados>(Oid);
                strTOOL_TIPO_RES = Tur.Empleado.NombreCompleto + " Incidencia " + Tur.Incidencia.Descripcion;
                return strTOOL_TIPO_RES;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }


    }
}
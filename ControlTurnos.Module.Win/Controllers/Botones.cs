﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using ControlTurnos.Module.BusinessObjects;
using ControlTurnos.Module.BusinessObjects.Temel;
//using ControlTurnos.Module.Repos;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;

namespace ControlTurnos.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Botones : ViewController
    {
        //private SimpleAction simpleAction1;
        //private SimpleAction simpleAction2;
        private System.ComponentModel.IContainer components;

        public Botones()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void DetalleContrato_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace os = View.ObjectSpace;

                ControlTurnos.Module.Win.Controllers.DetalleTurnos frmForm = new ControlTurnos.Module.Win.Controllers.DetalleTurnos(os, Application);
                frmForm.Show();


            }
            catch (Exception)
            {
                MessageBox.Show("Error.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DetalleContrato = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.AccionesContrato = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // DetalleContrato
            // 
            this.DetalleContrato.Caption = "Consultar detalle";
            this.DetalleContrato.ConfirmationMessage = null;
            this.DetalleContrato.Id = "GenerarDetalle";
            this.DetalleContrato.TargetObjectType = typeof(ControlTurnos.Module.BusinessObjects.TurnosEmpleados);
            this.DetalleContrato.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.DetalleContrato.ToolTip = null;
            this.DetalleContrato.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.DetalleContrato.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.DetalleContrato_Execute);
            // 
            // simpleAction2
            // 
            // 
            // AccionesContrato
            // 
            this.AccionesContrato.Caption = "Generar detalle";
            this.AccionesContrato.ConfirmationMessage = null;
            this.AccionesContrato.Id = "GenerarAcciones";
            this.AccionesContrato.TargetObjectType = typeof(ControlTurnos.Module.BusinessObjects.TurnosEmpleados);
            this.AccionesContrato.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.AccionesContrato.ToolTip = null;
            this.AccionesContrato.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.AccionesContrato.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.AccionesContrato_Execute);
            // 
            // Botones
            // 
            this.Actions.Add(this.DetalleContrato);
            this.Actions.Add(this.AccionesContrato);

        }

        private void AccionesContrato_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            //XPCollection<BusinessObjects.TurnosEmpleados> turnosEmpleados = new XPCollection<BusinessObjects.TurnosEmpleados>(((XPObjectSpace)this.View.ObjectSpace).Session);
            XPCollection<BusinessObjects.Temel.Employee> Empleados = new XPCollection<BusinessObjects.Temel.Employee>(((XPObjectSpace)this.View.ObjectSpace).Session);

            foreach (Employee Empleado in Empleados)
            {
                DateTime inicio = new DateTime(2019, 11, 1);
                DateTime fin = new DateTime(2019, 11, 30);
                DateTime temp = inicio;
                while (temp<fin)
                {
                    BusinessObjects.TurnosEmpleados turnosEmpleados = new BusinessObjects.TurnosEmpleados(((XPObjectSpace)this.View.ObjectSpace).Session);
                    turnosEmpleados.Empleado = Empleado;
                    turnosEmpleados.Fecha = temp;
                    turnosEmpleados.Cantidad = 8;
                    turnosEmpleados.HoraInicio= TimeSpan.FromTicks((new DateTime (1,1,1,8,0,0)).Ticks);
                    turnosEmpleados.HoraFin = TimeSpan.FromTicks((new DateTime(1,1, 1, 18, 0, 0)).Ticks);
                    turnosEmpleados.Incidencia = this.View.ObjectSpace.GetObjectByKey<BusinessObjects.Incidencia>(1);
                    turnosEmpleados.Save();

                    temp=temp.AddDays(1);
                }
            }
            this.View.ObjectSpace.CommitChanges();
        }

        private DevExpress.ExpressApp.Actions.SimpleAction DetalleContrato;
        private DevExpress.ExpressApp.Actions.SimpleAction AccionesContrato;
    }
}


﻿namespace ControlTurnos.Module.Win.Controllers
{
    partial class DetalleTurnos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DetalleTurnos));
            DevExpress.XtraScheduler.TimeRuler timeRuler1 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler2 = new DevExpress.XtraScheduler.TimeRuler();
            this.lblTrabajo_Status = new DevExpress.XtraEditors.LabelControl();
            this.coeTrabajo_Status = new DevExpress.XtraEditors.ColorEdit();
            this.lblVacaciones_Status = new DevExpress.XtraEditors.LabelControl();
            this.coeVacaciones_Status = new DevExpress.XtraEditors.ColorEdit();
            this.lblLibre_Status = new DevExpress.XtraEditors.LabelControl();
            this.coeLibre_Status = new DevExpress.XtraEditors.ColorEdit();
            this.lblBaja_Status = new DevExpress.XtraEditors.LabelControl();
            this.coeBaja_Status = new DevExpress.XtraEditors.ColorEdit();
            this.coeOtros_Status = new DevExpress.XtraEditors.ColorEdit();
            this.pnlInfo = new DevExpress.XtraEditors.PanelControl();
            this.btnFiltrar = new DevExpress.XtraEditors.SimpleButton();
            this.ttcRoomChart = new DevExpress.Utils.ToolTipController(this.components);
            this.btnReset = new DevExpress.XtraEditors.SimpleButton();
            this.btnFiltrar_Edit = new DevExpress.XtraEditors.SimpleButton();
            this.lblOtros_Status = new DevExpress.XtraEditors.LabelControl();
            this.schedulerStorage1 = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
            this.schRoomChart = new DevExpress.XtraScheduler.SchedulerControl();
            this.pnlRoomChart = new DevExpress.XtraEditors.PanelControl();
            this.applicationMenu1 = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.coeTrabajo_Status.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coeVacaciones_Status.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coeLibre_Status.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coeBaja_Status.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coeOtros_Status.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInfo)).BeginInit();
            this.pnlInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schRoomChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlRoomChart)).BeginInit();
            this.pnlRoomChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTrabajo_Status
            // 
            this.lblTrabajo_Status.Location = new System.Drawing.Point(88, 9);
            this.lblTrabajo_Status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblTrabajo_Status.Name = "lblTrabajo_Status";
            this.lblTrabajo_Status.Size = new System.Drawing.Size(45, 16);
            this.lblTrabajo_Status.TabIndex = 9;
            this.lblTrabajo_Status.Text = "Trabajo";
            // 
            // coeTrabajo_Status
            // 
            this.coeTrabajo_Status.EditValue = System.Drawing.Color.GreenYellow;
            this.coeTrabajo_Status.Enabled = false;
            this.coeTrabajo_Status.Location = new System.Drawing.Point(42, 4);
            this.coeTrabajo_Status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.coeTrabajo_Status.Name = "coeTrabajo_Status";
            this.coeTrabajo_Status.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.coeTrabajo_Status.Properties.Appearance.Options.UseBackColor = true;
            this.coeTrabajo_Status.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.coeTrabajo_Status.Size = new System.Drawing.Size(38, 20);
            this.coeTrabajo_Status.TabIndex = 8;
            // 
            // lblVacaciones_Status
            // 
            this.lblVacaciones_Status.Location = new System.Drawing.Point(207, 9);
            this.lblVacaciones_Status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblVacaciones_Status.Name = "lblVacaciones_Status";
            this.lblVacaciones_Status.Size = new System.Drawing.Size(64, 16);
            this.lblVacaciones_Status.TabIndex = 7;
            this.lblVacaciones_Status.Text = "Vacaciones";
            // 
            // coeVacaciones_Status
            // 
            this.coeVacaciones_Status.EditValue = System.Drawing.Color.Red;
            this.coeVacaciones_Status.Enabled = false;
            this.coeVacaciones_Status.Location = new System.Drawing.Point(162, 4);
            this.coeVacaciones_Status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.coeVacaciones_Status.Name = "coeVacaciones_Status";
            this.coeVacaciones_Status.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.coeVacaciones_Status.Properties.Appearance.Options.UseBackColor = true;
            this.coeVacaciones_Status.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.coeVacaciones_Status.Size = new System.Drawing.Size(38, 20);
            this.coeVacaciones_Status.TabIndex = 6;
            // 
            // lblLibre_Status
            // 
            this.lblLibre_Status.Location = new System.Drawing.Point(456, 9);
            this.lblLibre_Status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblLibre_Status.Name = "lblLibre_Status";
            this.lblLibre_Status.Size = new System.Drawing.Size(28, 16);
            this.lblLibre_Status.TabIndex = 5;
            this.lblLibre_Status.Text = "Libre";
            // 
            // coeLibre_Status
            // 
            this.coeLibre_Status.EditValue = System.Drawing.Color.IndianRed;
            this.coeLibre_Status.Enabled = false;
            this.coeLibre_Status.Location = new System.Drawing.Point(414, 4);
            this.coeLibre_Status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.coeLibre_Status.Name = "coeLibre_Status";
            this.coeLibre_Status.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.coeLibre_Status.Properties.Appearance.Options.UseBackColor = true;
            this.coeLibre_Status.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.coeLibre_Status.Size = new System.Drawing.Size(38, 20);
            this.coeLibre_Status.TabIndex = 4;
            // 
            // lblBaja_Status
            // 
            this.lblBaja_Status.Location = new System.Drawing.Point(357, 9);
            this.lblBaja_Status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblBaja_Status.Name = "lblBaja_Status";
            this.lblBaja_Status.Size = new System.Drawing.Size(25, 16);
            this.lblBaja_Status.TabIndex = 3;
            this.lblBaja_Status.Text = "Baja";
            // 
            // coeBaja_Status
            // 
            this.coeBaja_Status.EditValue = System.Drawing.Color.Yellow;
            this.coeBaja_Status.Enabled = false;
            this.coeBaja_Status.Location = new System.Drawing.Point(300, 4);
            this.coeBaja_Status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.coeBaja_Status.Name = "coeBaja_Status";
            this.coeBaja_Status.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.coeBaja_Status.Properties.Appearance.Options.UseBackColor = true;
            this.coeBaja_Status.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.coeBaja_Status.Size = new System.Drawing.Size(38, 20);
            this.coeBaja_Status.TabIndex = 2;
            // 
            // coeOtros_Status
            // 
            this.coeOtros_Status.EditValue = System.Drawing.Color.Orange;
            this.coeOtros_Status.Enabled = false;
            this.coeOtros_Status.Location = new System.Drawing.Point(526, 4);
            this.coeOtros_Status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.coeOtros_Status.Name = "coeOtros_Status";
            this.coeOtros_Status.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.coeOtros_Status.Properties.Appearance.Options.UseBackColor = true;
            this.coeOtros_Status.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.coeOtros_Status.Size = new System.Drawing.Size(38, 20);
            this.coeOtros_Status.TabIndex = 0;
            // 
            // pnlInfo
            // 
            this.pnlInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlInfo.Controls.Add(this.btnFiltrar);
            this.pnlInfo.Controls.Add(this.btnReset);
            this.pnlInfo.Controls.Add(this.btnFiltrar_Edit);
            this.pnlInfo.Controls.Add(this.lblTrabajo_Status);
            this.pnlInfo.Controls.Add(this.coeTrabajo_Status);
            this.pnlInfo.Controls.Add(this.lblVacaciones_Status);
            this.pnlInfo.Controls.Add(this.coeVacaciones_Status);
            this.pnlInfo.Controls.Add(this.lblLibre_Status);
            this.pnlInfo.Controls.Add(this.coeLibre_Status);
            this.pnlInfo.Controls.Add(this.lblBaja_Status);
            this.pnlInfo.Controls.Add(this.coeBaja_Status);
            this.pnlInfo.Controls.Add(this.lblOtros_Status);
            this.pnlInfo.Controls.Add(this.coeOtros_Status);
            this.pnlInfo.Location = new System.Drawing.Point(3, 554);
            this.pnlInfo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlInfo.Name = "pnlInfo";
            this.pnlInfo.Size = new System.Drawing.Size(1283, 37);
            this.pnlInfo.TabIndex = 6;
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnFiltrar.Appearance.Options.UseBackColor = true;
            this.btnFiltrar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnFiltrar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFiltrar.ImageOptions.Image")));
            this.btnFiltrar.Location = new System.Drawing.Point(903, 2);
            this.btnFiltrar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.Size = new System.Drawing.Size(92, 30);
            this.btnFiltrar.TabIndex = 123;
            this.btnFiltrar.Text = "Filtrar";
            this.btnFiltrar.ToolTip = "Aplicar Filtro";
            this.btnFiltrar.ToolTipController = this.ttcRoomChart;
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            // 
            // btnReset
            // 
            this.btnReset.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnReset.Appearance.Options.UseBackColor = true;
            this.btnReset.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnReset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnReset.ImageOptions.Image")));
            this.btnReset.Location = new System.Drawing.Point(1133, 4);
            this.btnReset.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(141, 30);
            this.btnReset.TabIndex = 122;
            this.btnReset.Text = "No Aplicar Filtros";
            this.btnReset.ToolTip = "No Aplicar Filtro";
            this.btnReset.ToolTipController = this.ttcRoomChart;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnFiltrar_Edit
            // 
            this.btnFiltrar_Edit.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnFiltrar_Edit.Appearance.Options.UseBackColor = true;
            this.btnFiltrar_Edit.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.btnFiltrar_Edit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFiltrar_Edit.ImageOptions.Image")));
            this.btnFiltrar_Edit.Location = new System.Drawing.Point(1001, 2);
            this.btnFiltrar_Edit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFiltrar_Edit.Name = "btnFiltrar_Edit";
            this.btnFiltrar_Edit.Size = new System.Drawing.Size(115, 30);
            this.btnFiltrar_Edit.TabIndex = 121;
            this.btnFiltrar_Edit.Text = "Editar Filtro";
            this.btnFiltrar_Edit.ToolTip = "Editar Filtro";
            this.btnFiltrar_Edit.ToolTipController = this.ttcRoomChart;
            this.btnFiltrar_Edit.Click += new System.EventHandler(this.btnFiltrar_Edit_Click);
            // 
            // lblOtros_Status
            // 
            this.lblOtros_Status.Location = new System.Drawing.Point(594, 9);
            this.lblOtros_Status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lblOtros_Status.Name = "lblOtros_Status";
            this.lblOtros_Status.Size = new System.Drawing.Size(51, 16);
            this.lblOtros_Status.TabIndex = 1;
            this.lblOtros_Status.Text = "Ausencia";
            // 
            // schedulerStorage1
            // 
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.Empty, "", "");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.GreenYellow, "Trabajo", "&Trabajo");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.Red, "Vacaciones", "&Vacaciones");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.IndianRed, "Dia Libre", "&Dia Libre");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.Yellow, "Enfermedad", "&Enfermedad");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.OrangeRed, "Ausencia", "&Ausencia");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.Orange, "Asuntos Propios", "&Asuntos Propios");
            // 
            // schRoomChart
            // 
            this.schRoomChart.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Timeline;
            this.schRoomChart.DataStorage = this.schedulerStorage1;
            this.schRoomChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.schRoomChart.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.schRoomChart.Location = new System.Drawing.Point(2, 2);
            this.schRoomChart.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.schRoomChart.Name = "schRoomChart";
            this.schRoomChart.OptionsBehavior.ShowRemindersForm = false;
            this.schRoomChart.OptionsCustomization.AllowAppointmentConflicts = DevExpress.XtraScheduler.AppointmentConflictsMode.Forbidden;
            this.schRoomChart.OptionsCustomization.AllowAppointmentCopy = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schRoomChart.OptionsCustomization.AllowAppointmentCreate = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schRoomChart.OptionsCustomization.AllowAppointmentDelete = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schRoomChart.OptionsCustomization.AllowDisplayAppointmentDependencyForm = DevExpress.XtraScheduler.AllowDisplayAppointmentDependencyForm.Never;
            this.schRoomChart.OptionsCustomization.AllowDisplayAppointmentForm = DevExpress.XtraScheduler.AllowDisplayAppointmentForm.Never;
            this.schRoomChart.OptionsPrint.PrintStyle = DevExpress.XtraScheduler.Printing.SchedulerPrintStyleKind.Daily;
            this.schRoomChart.OptionsView.ResourceHeaders.RotateCaption = false;
            this.schRoomChart.OptionsView.ToolTipVisibility = DevExpress.XtraScheduler.ToolTipVisibility.Always;
            this.schRoomChart.Size = new System.Drawing.Size(1279, 539);
            this.schRoomChart.Start = new System.DateTime(2019, 3, 4, 0, 0, 0, 0);
            this.schRoomChart.TabIndex = 1;
            this.schRoomChart.ToolTipController = this.ttcRoomChart;
            this.schRoomChart.Views.DayView.Enabled = false;
            timeRuler1.TimeZoneId = "Romance Standard Time";
            timeRuler1.UseClientTimeZone = false;
            this.schRoomChart.Views.DayView.TimeRulers.Add(timeRuler1);
            this.schRoomChart.Views.GanttView.CellsAutoHeightOptions.Enabled = true;
            this.schRoomChart.Views.GanttView.Enabled = false;
            this.schRoomChart.Views.MonthView.Enabled = false;
            this.schRoomChart.Views.TimelineView.Appearance.Appointment.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.schRoomChart.Views.TimelineView.Appearance.Appointment.Options.UseFont = true;
            this.schRoomChart.Views.TimelineView.Appearance.HeaderCaption.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.schRoomChart.Views.TimelineView.Appearance.HeaderCaption.Options.UseFont = true;
            this.schRoomChart.Views.TimelineView.Appearance.ResourceHeaderCaption.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.schRoomChart.Views.TimelineView.Appearance.ResourceHeaderCaption.Options.UseFont = true;
            this.schRoomChart.Views.TimelineView.AppointmentDisplayOptions.AppointmentInterspacing = 0;
            this.schRoomChart.Views.WeekView.Enabled = false;
            this.schRoomChart.Views.WorkWeekView.Enabled = false;
            timeRuler2.TimeZoneId = "Romance Standard Time";
            timeRuler2.UseClientTimeZone = false;
            this.schRoomChart.Views.WorkWeekView.TimeRulers.Add(timeRuler2);
            this.schRoomChart.AppointmentDrop += new DevExpress.XtraScheduler.AppointmentDragEventHandler(this.schRoomChart_AppointmentDrop);
            this.schRoomChart.AppointmentResized += new DevExpress.XtraScheduler.AppointmentResizeEventHandler(this.schRoomChart_AppointmentResized);
            this.schRoomChart.PopupMenuShowing += new DevExpress.XtraScheduler.PopupMenuShowingEventHandler(this.schRoomChart_PopupMenuShowing);
            this.schRoomChart.MouseMove += new System.Windows.Forms.MouseEventHandler(this.schRoomChart_MouseMove);
            this.schRoomChart.AppointmentViewInfoCustomizing += SchRoomChart_AppointmentViewInfoCustomizing;
            this.schRoomChart.CustomDrawAppointment += SchRoomChart_CustomDrawAppointment;
            this.schRoomChart.CustomizeAppointmentFlyout += SchRoomChart_CustomizeAppointmentFlyout;
            this.schRoomChart.CustomDrawAppointmentFlyoutSubject += SchRoomChart_CustomDrawAppointmentFlyoutSubject;
            this.schRoomChart.AppointmentFlyoutShowing += SchRoomChart_AppointmentFlyoutShowing;
            // 
            // pnlRoomChart
            // 
            this.pnlRoomChart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlRoomChart.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.pnlRoomChart.Controls.Add(this.schRoomChart);
            this.pnlRoomChart.Location = new System.Drawing.Point(3, 4);
            this.pnlRoomChart.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlRoomChart.Name = "pnlRoomChart";
            this.pnlRoomChart.Size = new System.Drawing.Size(1283, 543);
            this.pnlRoomChart.TabIndex = 4;
            // 
            // applicationMenu1
            // 
            this.applicationMenu1.Name = "applicationMenu1";
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1289, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 597);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1289, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 597);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1289, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 597);
            // 
            // DetalleTurnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1289, 597);
            this.Controls.Add(this.pnlInfo);
            this.Controls.Add(this.pnlRoomChart);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "DetalleTurnos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmRoomChart";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmDetalleTurnos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.coeTrabajo_Status.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coeVacaciones_Status.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coeLibre_Status.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coeBaja_Status.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coeOtros_Status.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlInfo)).EndInit();
            this.pnlInfo.ResumeLayout(false);
            this.pnlInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schRoomChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlRoomChart)).EndInit();
            this.pnlRoomChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

       






        #endregion
        private DevExpress.XtraEditors.LabelControl lblTrabajo_Status;
        private DevExpress.XtraEditors.ColorEdit coeTrabajo_Status;
        private DevExpress.XtraEditors.LabelControl lblVacaciones_Status;
        private DevExpress.XtraEditors.ColorEdit coeVacaciones_Status;
        private DevExpress.XtraEditors.LabelControl lblLibre_Status;
        private DevExpress.XtraEditors.ColorEdit coeLibre_Status;
        private DevExpress.XtraEditors.LabelControl lblBaja_Status;
        private DevExpress.XtraEditors.ColorEdit coeBaja_Status;
        private DevExpress.XtraEditors.ColorEdit coeOtros_Status;
        private DevExpress.XtraEditors.PanelControl pnlInfo;
        private DevExpress.XtraEditors.LabelControl lblOtros_Status;
        private DevExpress.XtraScheduler.SchedulerStorage schedulerStorage1;
        private DevExpress.XtraScheduler.SchedulerControl schRoomChart;
        private DevExpress.XtraEditors.PanelControl pnlRoomChart;
        private DevExpress.XtraEditors.SimpleButton btnReset;
        private DevExpress.XtraEditors.SimpleButton btnFiltrar_Edit;
        private DevExpress.Utils.ToolTipController ttcRoomChart;
        private DevExpress.XtraEditors.SimpleButton btnFiltrar;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
    }
}